<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>

<body>
    <h1><b> Buat Account Baru</b></h1>
    <h3><b> Sign Up Form </b></h3>

    <form action="/welcome" method="get">
        @csrf

        <p>First Name</p>
        <input type="text" name="firstname">

        <p>Last Name</p>
        <input type="text" name="lastname">

        <p>Gender</p>
        <input type="radio" name="gender"> Male<br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other

        <p>Nationality</p>
        <select name="" id="">
            <option value="">Indonesia</option>
            <option value="">Amerika</option>
            <option value="">Other</option>
        </select>

        <p>Language Spoken</p>
        <input type="checkbox" name="lagnuage">Bahasa Indonesia <br>
        <input type="checkbox" name="lagnuage">English <br>
        <input type="checkbox" name="lagnuage">Other


        <p>Bio:</p>
        <textarea cols="30" rows="8"></textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>




</body>

</html>