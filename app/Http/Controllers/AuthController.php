<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('contoh1.form');
    }

    public function welcome(Request $request)
    {
        $firstname = $request->firstname;
        $lastname = $request->lastname;

        return view('contoh1.welcome', compact('firstname', 'lastname'));
    }
}
